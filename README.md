[![](https://jitpack.io/v/com.gitlab.farmaturn/farmaturn-model.svg)](https://jitpack.io/#com.gitlab.farmaturn/farmaturn-model)

[![coverage report](https://gitlab.com/farmaturn/farmaturn-services/badges/master/coverage.svg)](https://farmaturn.gitlab.io/farmaturn-services/code-coverage-reports)

# REPOSITORIOS

https://gitlab.com/farmaturn/farmaturn-model  <br />
https://gitlab.com/farmaturn/farmaturn-services  <br />
https://gitlab.com/farmaturn/farmaturn-view <br />